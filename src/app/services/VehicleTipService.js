import Rest from './Rest'

/**
 * @typedef {VehicleTipService}
 */
export default class VehicleTipService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/vehicles-tips'

    formatDate(fullDate){
      if (!fullDate) return null;
      let date = new Date(fullDate).toISOString().substr(0, 10);
      const[year, month, day] = date.split('-');
      return `${day}.${month}.${year}`;
    }
}
