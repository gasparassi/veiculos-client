import Rest from '../Rest'

/**
 * @typedef {UserVehicleTipService}
 */
export default class UserVehicleTipService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/users/vehicle-tips'

}
