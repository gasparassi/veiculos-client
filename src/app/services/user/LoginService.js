import Rest from '../Rest';

/**
 * @typedef {LoginService}
 */
export default class LoginService extends Rest {
	/**
     * @type {String}
     */
	static resource = '/users/login';

	login(record) {
		return this.create(record).then((response) => {
			if ( response.user.access.token ) {
				localStorage.setItem('user', JSON.stringify(response));
			}
			return response;
		});
	}
}
