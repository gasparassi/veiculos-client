import Rest from '../Rest'

/**
 * @typedef {RegisterService}
 */
export default class RegisterService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/users/register'

    register (record) {
        return this.create(record);
    }
}