export default function authHeader( access ) {
  if ( access !== null ) {
    return `${access.token_type}${access.token}`;
  } else {
    return {};
  }
}
