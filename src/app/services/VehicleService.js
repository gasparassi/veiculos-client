import Rest from './Rest'

/**
 * @typedef {VehicleService}
 */
export default class VehicleService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/vehicles'

}
