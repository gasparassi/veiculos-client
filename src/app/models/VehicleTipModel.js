import VehicleModel from './VehicleModel'
/**
 * Vehicle Tip Model
 */

export default class VehicleTipModel {

  id = null;
  create_at = "";
  user_id = null;
  vehicle_id = null;

  constructor(user_id, vehicle_id, create_at) {
    this.user_id = user_id;
    this.create_at = create_at;
    this.vehicle_id = vehicle_id;
  }
}
