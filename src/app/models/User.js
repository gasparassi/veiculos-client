/**
 * User Model
 */

export default class UserModel {

  id = null;
  name = "";
  email = "";
  password = "";
  password_confirmation = "";
  status = "Ativo";

  constructor(name, email, password, password_confirmation) {
    this.name = name,
    this.email = email;
    this.password = password;
    this.password_confirmation = password_confirmation;
  }

  rules = {
    required: (value) => !!value || "Este campo é obrigatório",
    minCaracterPassword: (value) => (value && value.length >= 6) || "O campo senha precisa ter no mínimo 6 caracteres",
    emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
  }

  validatorConfirmPassword(password, passwordConfirmation){
    return [
      password === passwordConfirmation || "O campo senha e confirme sua senha não são iguais",
    ];
  }

}
