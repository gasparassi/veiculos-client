/**
 * Vehicle Model
 */

export default class VehicleModel {

  id = null;
  type = "";
  mark = "";
  model = "";
  version = "";

  constructor(type, mark, model, version) {
    this.type = type,
    this.mark = mark,
    this.model = model;
    this.version = version;
  }
}
