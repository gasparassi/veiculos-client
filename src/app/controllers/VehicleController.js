import VehicleService from '../services/VehicleService';
import VehicleModel from '../models/VehicleModel';

/**
 * @typedef {VehicleTipController}
 */
export default class VehicleTipController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = VehicleService.build();
    }

    vehicleModel(){
        return new VehicleModel();
    }

    create( vehicle ) {
        return this.service.create(vehicle).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response;
            }
        );
    }

    search (user) {
      return this.service.read(user).then(
        response => {
          return response;
        }
      );
    }

    getOne(id){
        return this.service.read(id).then(
            response => {
                return response;
            }
        );
    }

    delete( vehicle ) {
      return this.service.destroy(vehicle).then(
            response => {
                return response;
            }
        );
    }

    update( vehicle ) {
       return this.service.update(vehicle).then(
            response => {
                return response;
            }
        );
    }

    rules = {
        required: (value) => !!value || "Este campo é obrigatório",
        maxLength: (value) => (value && value.length < 100) || "Máximo de caracteres permitidos",
    }

}
