import UserService from '../services/user/UserService';
import UserVehicleTipService from '../services/user/UserVehicleTipService';

/**
 * @typedef {UserController}
 */
export default class UserController {

    /**
    * @type {null}
     */

    service = null;

    serviceTip = null;

    constructor(){
        this.service = UserService.build();
        this.serviceTip = UserVehicleTipService.build();
    }

    create( user ) {
        return this.service.create(user).then(
            response => {
                return response;
            }
        );
    }

    getAllVehicleTipsFromUser( user_id ) {
      return this.serviceTip.read(user_id).then(
        response => {
          return response;
        }
      );
    }

}
