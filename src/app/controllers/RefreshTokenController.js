import RefreshTokenService from '../services/user/RefreshTokenService';

/**
 * @typedef {RefreshTokenController}
 */
export default class RefreshTokenController {
	/**
    * @type {null}
     */

	service = null;

	constructor() {
		this.service = RefreshTokenService.build();
	}

	refreshToken(token) {
		return this.service.refresh(token).then((response) => {
			console.log('response controller: ', response);
			return response;
		});
	}
}
