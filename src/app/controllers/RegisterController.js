import RegisterService from '../services/user/RegisterService'

/**
 * @typedef {RegisterController}
 */
export default class RegisterController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = RegisterService.build();
    }

    create( user ) {
        return this.service.create(user).then(
            response => {
                return response;
            }
        );
    }
    
}