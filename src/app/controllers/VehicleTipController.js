import VehicleService from '../services/VehicleTipService';
import VehicleTipModel from '../models/VehicleTipModel'

/**
 * @typedef {VehicleTipController}
 */
export default class VehicleTipController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = VehicleService.build();
    }

    vehicleTipModel(){
        return new VehicleTipModel();
    }

    create( vehicleTip ) {
        return this.service.create(vehicleTip).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response;
            }
        );
    }

    search (user) {
      return this.service.read(user).then(
        response => {
          return response;
        }
      );
    }

    getOne(id){
        return this.service.read(id).then(
            response => {
                return response;
            }
        );
    }

    delete( tip ) {
      return this.service.destroy(tip).then(
            response => {
                return response;
            }
        );
    }

    update( tip ) {
       return this.service.update(tip).then(
            response => {
                return response;
            }
        );
    }

    rules = {
        required: (value) => !!value || "Este campo é obrigatório",
        maxLength: (value) => (value && value.length < 100) || "Máximo de caracteres permitidos",
    }

}
