import axios from 'axios';
import authHeader from '../../services/user/AuthHeader';

/**
 * @typedef {HttpClient}
 */
export default class HttpClient {
	baseUrl = 'http://localhost:8000';
	timeOut = 10000;
	standard = null;

	constructor() {
		this.standard = axios.create({
			baseURL: this.baseUrl,
			timeout: this.timeOut,
			transformResponse: [
				function(data) {
					return data;
				}
			]
		});

		this.interceptorsRequest();

		this.interceptorsResponse();
	}

	interceptorsRequest() {
		this.standard.interceptors.request.use(
			(config) => {
				if (
          !config.url.endsWith('#/') ||
					!config.url.endsWith('login') ||
					!config.url.endsWith('register')
				) {
					config.headers['Content-Type'] = 'application/json';

					const storage = JSON.parse(localStorage.getItem('user'));

					if (storage !== null) {
						config.headers.Authorization = authHeader(storage.user.access);
					}

				}
				return config;
			},
			(error) => {
				console.error('ERRO REQUEST: ', error.message);
				return Promise.reject(error);
			}
		);
	}

	interceptorsResponse() {
		this.standard.interceptors.response.use(
			(response) => {
				return response;
			},
			(error) => {
				switch (error.response.status) {
					case 400:
						console.error(error.response.status, error.message);
						break;
					case 403:
						console.error(error.response.status, error.message);
						break;
					case 404:
						console.error(error.response.status, error.message);
						break;

					default:
						break;
				}
				return Promise.reject(error);
			}
		);
	}
}
