import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},

				// Pages auth not required
				{
					name: 'sign-in',
					path: 'login',
					component: () => import('@/views/pages/user/Login')
				},
				{
					name: 'sign-up',
					path: 'pages/user/register',
					component: () => import('@/views/pages/user/Register')
				},
        	// Pages with auth required
        {
					name: 'vehicle-list',
					path: 'pages/vehicle/index',
					component: () => import('@/views/pages/vehicle/Vehicles'),
          meta: {
            requiresAuth: true
          }
				},
        {
					name: 'vehicle-form',
					path: 'pages/vehicle/index',
					component: () => import('@/views/pages/vehicle/VehicleForm'),
          meta: {
            requiresAuth: true
          }
				},
        {
					name: 'vehicle-tips-list-user',
					path: 'pages/vehicle-tip/user/index',
					component: () => import('@/views/pages/vehicle-tip/VehicleTips'),
          meta: {
            requiresAuth: true
          }
				},
        {
					name: 'vehicle-tips-form',
					path: 'pages/vehicle-tip/user/register',
					component: () => import('@/views/pages/vehicle-tip/VehicleTipForm'),
          meta: {
            requiresAuth: true
          }
				},
			]
		}
	]
});

router.beforeEach((to, from, next) => {

	const user_logged = JSON.parse(localStorage.getItem('user'));

	if (to.matched.some((record) => record.meta.requiresAuth)) {
		if (user_logged == null) {
			next({
				path: '/login',
				params: { nextUrl: to.fullPath }
			});
		} else {
			if (to.matched.some((record) => record.meta.is_admin)) {
				if (user.profile.is_admin == 1) {
					next();
				} else {
					next({ name: 'dashboard' });
				}
			} else {
				next();
			}
		}
	} else if (to.matched.some((record) => record.meta.guest)) {
		if (user_logged == null) {
			next();
		} else {
			next({ name: 'dashboard' });
		}
	} else {
		next();
	}
});

export default router;
